const activityService = require('./../service/activity');

const createActivity = async (req, res, next) => {
    const activity = req.body;
    if(activity.activityID) {
        const result = await activityService.create(activity);
        res.status(201).send({
            message: 'activity added successfully.'
        });
    } else {
        res.status(400).send({
            message: 'Invalid activity payload.'
        });
    }
}

const getAllActivities = async (req, res, next) => {
    try {
        const activities = await activityService.getAll();
        res.status(200).send(activities);
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }
}

module.exports = {
    createActivity,
    getAllActivities,
}