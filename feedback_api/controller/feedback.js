const feedbackService = require('./../service/feedback');

const createFeedback = async (req, res, next) => {
    const feedback = req.body;
    if(feedback.reaction && feedback.activityCode) {
        const result = await feedbackService.create(feedback);
        res.status(201).send({
            message: 'feedback added successfully.'
        });
    } else {
        res.status(400).send({
            message: 'Invalid feedback payload.'
        });
    }
}

const getAllFeedbacks = async (req, res, next) => {
    try {
        const feedbacks = await feedbackService.getAll();
        res.status(200).send(feedbacks);
    } catch(err) {
        console.log("AICI");
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }
}

const getFeedbacksByActivityCode = async (req, res, next) => {
    try {
        const activityCode = req.body.activityCode;
        if(activityCode) {
            try {
                const feedbacks = await feedbackService.getByActivityCode(activityCode);
                res.status(200).send(feedbacks);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No reaction specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

module.exports = {
    createFeedback,
    getAllFeedbacks,
    getFeedbacksByActivityCode
}