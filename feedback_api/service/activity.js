const { Activity } = require('./../models/activity');

const activity = {
    create: async (activity) => {
        try {
            const result = await Activity.create(activity);
            return result;    
        } catch(err) {
           throw new Error(err.message); 
        }
    },
    getAll: async () => {
        try {
            const activities = await Activity.findAll();
            return activities;
        } catch(err) {
            console.log("AICI");
            throw new Error(err.message);
        }
    }
}

module.exports = activity;